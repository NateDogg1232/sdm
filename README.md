# Simple Display Manager (SDM)

A very simple "display manager" which is actually just a simple dialog box

SDM is written in 100% POSIX shell, meaning that (irregularities witholding), it should run on any *NIX system.
SDM is so simple it is written in one singular .sh file.
This .sh file is written in very well commented code, and should be simple to read.

## How to install:

1. Put SDM in your path
2. Make some entries and an init file (See How to use)
3. run simpledm.sh in your .profile
4. Have fun!

## Dependencies:

- A POSIX shell interpreter (Your *NIX should have this. I recommend `dash`)
- dialog
- coreutils (ls, clear, awk, etc.) 
- nl
- xargs

## How to use:

SDM is super simple to use.
You can read the massive docs in the source code to figure out more, but here's a super simple breakdown of how to use it

### Find the SDM entries directory

SDM will, by default, set its own entries directory to `$XDG_CONFIG_HOME/sdm` (`~/.config/home`).
You can set this in the environment beforehand by setting the `$SDM_ENTRY_DIR` variable.

### Set up `sdminit.sh`
`sdminit.sh` is a file which is run right as sdm starts.
This file is run in the same context as SDM is, meaning it is  NOT sandboxed when it is run, and is security sensitive.
This file should contain any configuration changes you want to make.
If you want to change any configuration, I recommend you read the various variables in the file, as it is most likely more up to date than this document, and it explains very clearly what each variable does.

This file can be safely left empty, but I very highly recommend at least setting `TEMP_DIR` using `mktemp`. An example `sdminit.sh` is left below to do exactly that

```sh
# sdminit.sh
TEMP_DIR="$(mktemp -d)"
```

### Create some entries

To make an SDM entry, create a directory inside the entries directory (THAT CONTAINS NO SPACES).
Within an entry directory, there must be three files: info, run, and stop.
- `info` must contain two variables. `ENT` which is a simple name for it, and `NAME` which is a human-readable name for the entry.
- `run` is run when the entry is selected. This can contain whatever you need to start that entry (eg. `xinit`)
- `stop` is run when `run` has finished. This can contain whatever cleanup you need afterwards. This can be safely left empty.

Here is an example entry directory to start a tmux session in the tty:

```sh
# info

ENT="tmux"
NAME="New TMUX session"
```

```sh
# run
tmux
```

```sh
# stop
```

### Run `simpledm.sh`

`simpledm.sh` should be in your path. I recommend running it in your `.bashrc` or `.profile`.

```sh
# ~/.profile
#...
exec simpledm.sh
```
