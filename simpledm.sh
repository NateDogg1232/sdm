#!/bin/sh

# Simple Display Manager (SDM): A super duper simple display manager. Basically
# an overglorified menu.

# Some sections to check out. Use your search function in your
# text editor to find them:
# - SDM_ENTRY_INFO
# - CONFIG

# Implementation sections:
# - LOCAL_VARS
# - ERROR_HANDLER
# - LOAD_INIT
# - CREATE_LOGDIR
# - FUNCTIONS
# - MAIN


#================
# SDM_ENTRY_INFO
#================

# IMPORTANT:
# SDM entries CANNOT have spaces in the directory name.
# This will cause errors in the dialog box nd will not
# work.


# SDM entries consist of three files:
# 1.) run
# 2.) stop
# 3.) info

# The run file is run when the entry is started.
# The stop file is run when the entry ends.
# The info file is run to get information about the
# entry

#-----
# run

# The run file is an extremely simple script.
# All it needs to do is exec into whatever the entry
# needs to run. DO NOT daemonize whatever process the
# entry needs to run. This script needs to be blocking
# and only end when the entry's purpose ends.
# It is given one singular environment variable and that
# is the $ENT environment variable given in info.
# This makes it easier to just simply link different
# variations of various DEs' or WMs' run scripts and
# just do some if statements on the $ENT variable

#------
# stop

# OPTIONAL
# The stop file is also a simple script and only exists
# to separate the run and the stop ideas. It just runs
# when the run script is done.
# Once again: DO NOT daemonize whatever processes need to
# be done. This needs to block until everything it needs to
# finish gets finished.

#------
# info

# The info script needs to set up a few key things. These
# are set up as environment variables and are REQUIRED. The
# entry will NOT show up if all these things are not set.
# These environment variables need to be non-zero values and

# $ENT
# Will give a much shorter, technical name of the entry

# $NAME
# Will give a better, pretty name.

# You can think of the difference of those two as the difference
# between the 7z executable name and the 7-Zip name.



#========
# CONFIG
#========

# We'll set up some defaults here first
# These WILL override any existing env vars,
# so make sure they are overriden again either
# after the wm is started, or in the sdminit.sh
# file
# These variables will always be all uppercase


# This is REQUIRED so we can find our sdminit.sh file.
# This is the ONLY variable that can exist in the env
# and not be overwritten
# This will contain our various dm entries in their own
# folders.
SDM_ENTRY_DIR=${SDM_ENTRY_DIR:-${XDG_CONFIG_HOME:-$HOME/.config/}/sdm}

# This is a temporary directory that can be used by
# SDM. I recommend making a subdirectory inside
# /tmp for it, as it'll create a few files.
# You can use maketmp in your sdminit.sh to do
# that
TEMP_DIR="/tmp"

# This should point to a directory where scripts will
# be run at the start of SDM. Keep blank if you
# do not want any to be run.
# The scripts in here should be arranged in the order
# you want them to run in alphabetical order. It's
# recommended that you number them with "00-", "01-", etc.
# so they run in the correct order.
INIT_SCRIPTS="$LOCAL/etc/init.d"

# This should point to a directory where scripts will be
# run after SDM logs you out.
# The scripts should be arranged just as INIT_SCRIPTS
# are arranged, so take a look at those
LOGOUT_SCRIPTS="$LOCAL/etc/logout.d"

# Set this to a non-empty value to make sure that we are
# not currently in a chroot.
# You can check out how this is done in the is_in_chroot
# function.
# If this is done, it will run whichever dm is named
# "in-chroot" if you are currently running inside a chroot
# Here comes the living documentation part. In order for
# this to work, you need to set an $ISCHROOT env variable
# to a non-zero value for it to know you are in a chroot.
SKIP_IF_CHROOT=yes

# Set this to a non-empty value to make sure that we are
# not already logged into an entry.
# You can check out how this is done in the is_logged_in
# function.
# If this is done, it will run whatever SDM entry named
# "logged-in" if you are currently logged in.
# Here comes the living documentation part again
# This is done by exporting ISLOGGEDIN=true. It will then
# check if this env variable is set and bail out if not
SKIP_IF_LOGGED_IN=yes

# Set this to a directory to store logfiles. They will
# be named accordingly to the current operation. Just
# ls the directory after running one to see what files
# it produces.
# SDM will try to create the directory for you
LOG_DIRECTORY="$LOCAL/log/sdm"

# Set this to a directory to keep the state of your system.
# This directory should already be created and SDM WILL error
# in mysterious ways, or it will do crazy stuff if not.
# PLEASE PLEASE make sure this directory is created.
# There are very few files created so it's safe to not create
# a directory for this reason.
STATE_DIR="$XDG_RUNTIME_DIR"

#============
# LOCAL_VARS
#============
# These variables will store state and other various things
# These variables will have the first letter capitalized
Log_dir_setup="false"


#===============
# ERROR_HANDLER
#===============
# This function provides a simpler way to do error handling

error_handler() {
    if [ "$Log_dir_setup" -eq "true" ]; then
        echo "$1" >> "$LOG_DIRECTORY"/sdm.err
    else
        echo "$1"
    fi
    echo "Dropping to sh"
    sh
    exit 1
}

#===========
# LOAD_INIT
#===========

# Let's first load up our ini file.
# Luckily, I have made this file super simple. It's just an sh file.
# It'll be run from here. If this doesn't exist, or errors out, SDM
# will error out and simply run sh.

. $SDM_ENTRY_DIR/sdminit.sh || error_handler "Failed to run $SDM_ENTRY_DIR/sdminit.sh"

#================
# CREATE_LOG_DIR
#================

# Then we'll try to create the log directory
if [ ! -d "$LOG_DIRECTORY" ]; then
    mkdir "$LOG_DIRECTORY" || error_handler "Error creating log directory $LOG_DIRECTORY"
fi

#===========
# FUNCTIONS
#===========

# Let's also define some functions that will be used throughout.
# This will also include some single-use ones that I just think
# are better read on their own


is_in_chroot() {
    [ -n "$ISCHROOT" ]
}
is_logged_in() {
    [ "$ISLOGGEDIN" -eq "true" ]
}

# Will run a directory ($1) full of scripts
# to run and output the output of these
# into two log files ($2) *.err and *.std
run_script_dir() {
    echo "Running $2"
    for s in "$1"/*; do
        echo "Running $s"
        sh $s >> "$LOG_DIRECTORY"/"$2".std 2>> "$LOG_DIRECTORY"/"$2".err
    done
}
run_init_scripts() {
    if [ ! -e "$STATE_DIR/init" ]; then
        run_script_dir "$INIT_SCRIPTS" "init"
        echo 1 > "$STATE_DIR/init"
    fi
}
run_logout_scripts() {
    if [ -e "$STATE_DIR/init" ]; then
        run_script_dir "$LOGOUT_SCRIPTS" "logout"
        rm "$STATE_DIR/init"
    fi
}

# The argument given will be the name of the directory to run
run_sdm_entry() {
    export ISLOGGEDIN=true
    . "$1"/info
    ENT="$ENT" "$1"/run >> "$LOG_DIRECTORY"/"$ENT".run.std 2>> "$LOG_DIRECTORY"/"$ENT".run.err
    ENT="$ENT" "$1"/stop >> "$LOG_DIRECTORY"/"$ENT".stop.std 2>> "$LOG_DIRECTORY"/"$ENT".stop.err
}

# Will output each entry along with a number for use
# by the system. It will be put in $TEMP_DIR/dirs
number_entries() {
    # Get all the directories but not the files
    # in the $SDM_ENTRY_DIR
    ls -d -L "$SDM_ENTRY_DIR"/*/ |
        # ls -d will output the directories with
        # a final slash at the end, so we need to remove it
        sed 's_/$__' |
        # And we'll use nl to number all the lines
        nl -w 2 -n "rz" > "$TEMP_DIR"/dirs
}

# Will output each entry number's name into
# $TEMP_DIR/names
get_entries_info() {
    # Read the "$TEMP_DIR"/dirs file line by line
    while IFS='' read -r line; do
        # Extract the number and entry directory
        local number=$(echo $line | awk -e ' { print $1 } ')
        local entry=$(entry_number $number)
        # Get the info from this entry
        . "$entry"/info
        # Separates the entry number and the name by a null character.
        # This will make it so that we can have spaces in our name
        # without messing up any later parsing.
        printf '%s\0%s\0' "$number" "$NAME" >> "$TEMP_DIR"/names
    done < "$TEMP_DIR"/dirs
}

# Will output the entry of the number ($1) given in
# $TEMP_DIR/dirs
entry_number() {
    grep "^$1" $TEMP_DIR/dirs | sed -e 's/^[0-9]*//' -e 's/^\s*//'
}

# Will get the entries in "$TEMP_DIR"/names and will
# output a selection to "$TEMP_DIR"/dmout.
do_dialog() {
    cat "$TEMP_DIR/names" | xargs -0 dialog --no-cancel --erase-on-exit --title "Simple Display Manager" --menu "Choose an entry to start" "0" "0" "0" 2> "$TEMP_DIR"/dmout
    clear
}

# Will run whatever entry was selected in do_dialog
run_dialog_out() {
    run_sdm_entry $(entry_number $(cat "$TEMP_DIR"/dmout))
}

#======
# MAIN
#======


# This is where the magic happens

if is_logged_in; then
    if [ -n "$SKIP_IF_LOGGED_IN" ]; then
        run_sdm_entry "$SDM_ENTRY_DIR/logged-in"
        exit 0
    fi
fi
if is_in_chroot; then
    if [ -n "$SKIP_IF_CHROOT" ]; then
        run_sdm_entry "$SDM_ENTRY_DIR/in-chroot"
        exit 0
    fi
fi
run_init_scripts
number_entries
get_entries_info
do_dialog
run_dialog_out
run_logout_scripts

# And now we clean up whatever we need to clean up
rm -r "$TEMP_DIR"
